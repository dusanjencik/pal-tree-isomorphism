package pal;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 * @created 08.11.15
 */
public class Graphviz {
    private StringBuilder stringBuilder;
    private int i;

    public Graphviz(int i) {
        this.i = i;
        stringBuilder = new StringBuilder();
    }

    public void add(String str) {
        stringBuilder.append(str);
    }

    public void clear() {
        stringBuilder = new StringBuilder();
    }

    public void show() {
        try {
            String all = "digraph G {\n" +
                    "nodesep=0.3;\n" +
                    "ranksep=0.2;\n" +
                    "forcelabels=true;\n" +
                    "margin=0.1;\n" +
                    "node [shape=circle];\n" +
                    "node [style=filled];\n" +
                    "edge [arrowsize=0.8];\n" + stringBuilder.toString() + "}\n";
            PrintWriter out = new PrintWriter("tmpGraph" + i + ".txt");
            out.print(all);
            out.close();
            Process p = Runtime.getRuntime().exec(new String[]{"bash","-c","dot -Tpng tmpGraph" + i + ".txt > outputGraph" + i + ".png"});
            if(p.waitFor() == 0){
                Runtime.getRuntime().exec(new String[]{"bash","-c","open outputGraph" + i + ".png"});
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
