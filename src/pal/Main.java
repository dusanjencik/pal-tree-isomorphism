package pal;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {

    static final int CONSISTENT_DEGREE = -1;

    static int N;
    static Vertex[] vertexesG1, vertexesG2;

    static TreeSet<Integer> solution;
    static Graphviz graphviz1 = new Graphviz(1), graphviz2 = new Graphviz(2);
    static boolean DEBUG = true;
    static boolean SHOW_GRAPHS = false;
    static int GRAPHVIS_VERSION = 1;

    static Comparator<Integer> treeReverseComparator = new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
            return o2.compareTo(o1);
        }
    };

    @SuppressWarnings("Duplicates")
    public static void main(String[] args) {
        // --- LOADING ---
        long time = System.nanoTime();
        String rightAnswer = null;
        Scanner in;
        if (args.length > 0) {
            try {
                in = new Scanner(new File(args[0]));
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
                in = new Scanner(System.in);
            }
        } else in = new Scanner(System.in);

        solution = new TreeSet<Integer>();
        N = in.nextInt();
        vertexesG1 = new Vertex[N];
        vertexesG2 = new Vertex[N + 1];
        for (int i = 0; i < N - 1; ++i) {
            int input = in.nextInt();
            int output = in.nextInt();
            if (vertexesG1[input] == null) vertexesG1[input] = new Vertex(input);
            if (vertexesG1[output] == null) vertexesG1[output] = new Vertex(output);
            vertexesG1[input].addOutput(output);
            vertexesG1[output].addOutput(input);
            if (SHOW_GRAPHS && GRAPHVIS_VERSION == 1) {
                graphviz1.add(String.format("\"%d\" -> \"%d\";\n", input, output));
                graphviz1.add(String.format("\"%d\" -> \"%d\";\n", output, input));
            }
        }
        for (int i = 0; i < N; ++i) {
            int input = in.nextInt();
            int output = in.nextInt();
            if (vertexesG2[input] == null) vertexesG2[input] = new Vertex(input);
            if (vertexesG2[output] == null) vertexesG2[output] = new Vertex(output);
            vertexesG2[input].addOutput(output);
            vertexesG2[output].addOutput(input);
            if (SHOW_GRAPHS && GRAPHVIS_VERSION == 1) {
                graphviz2.add(String.format("\"%d\" -> \"%d\";\n", input, output));
                graphviz2.add(String.format("\"%d\" -> \"%d\";\n", output, input));
            }
        }
        // --- END LOADING ---
        if (DEBUG) {
            if (args.length > 0) {
                try {
                    Scanner in2 = new Scanner(new File(args[0].replace("in", "out")));
                    rightAnswer = in2.nextLine();
                    String[] line = rightAnswer.split(" ");
                    in2.close();
                    for (String s : line) {
                        graphviz2.add(String.format("\"%s\" [color=\"0.000 1.000 1.000\"];\n", s));
                    }
                } catch (FileNotFoundException ignored) {
                }
            }
        }

        // computing centers of graphs
        HashSet<Integer> roots1 = buildTree(vertexesG1, N);
        HashSet<Integer> roots2 = buildTree(vertexesG2, N + 1);

        if (DEBUG && SHOW_GRAPHS) {
            for (int r : roots1) {
                if (GRAPHVIS_VERSION != 1) setUpTreeGraphviz(graphviz1, vertexesG1, r);
                graphviz1.add(String.format("\"%d\" [color=\"0.500 0.500 1.000\"];\n", r));
            }
            for (int r : roots2) {
                if (GRAPHVIS_VERSION != 1) setUpTreeGraphviz(graphviz2, vertexesG2, r);
                graphviz2.add(String.format("\"%d\" [color=\"0.500 0.500 1.000\"];\n", r));
            }
        }


        if (roots1.size() < roots2.size()) {
            // trying for G1:{0} <-> G2{0, 1}
            Iterator<Integer> it2 = roots2.iterator();
            int b1 = it2.next();
            int b2 = it2.next();
            vertexesG2[b2].addCertificateCandidate(b1);
            Iterator<Integer> it1 = roots1.iterator();
            int a1 = it1.next();
            String originalCert = vertexesG2[b2].certificate;
            updateCertificate(vertexesG2, b2);
            findDuplicates(a1, b2);
            vertexesG2[b2].certificate = originalCert;
            vertexesG2[b2].children.remove(new Integer(b1));
            vertexesG2[b1].children.add(b2);
            updateCertificate(vertexesG2, b1);
            findDuplicates(a1, b1);
        } else if (roots1.size() > roots2.size()) {
            // trying for G1:{0, 1} <-> G2{0}
            Iterator<Integer> it1 = roots1.iterator();
            int a1 = it1.next();
            int a2 = it1.next();
            vertexesG1[a2].addCertificateCandidate(a1);
            Iterator<Integer> it2 = roots2.iterator();
            int b1 = it2.next();
            String originalCert = vertexesG1[a2].certificate;
            updateCertificate(vertexesG1, a2);
            findDuplicates(a2, b1);
            vertexesG1[a2].certificate = originalCert;
            vertexesG1[a2].children.remove(new Integer(a2));
            vertexesG1[a1].children.add(a2);
            updateCertificate(vertexesG1, a1);
            findDuplicates(a1, b1);
        } else { // roots1.size() == roots2.size()
            for (int root1 : roots1) {
                for (int root2 : roots2) {
                    if (Math.abs(vertexesG1[root1].getChildNodes() - vertexesG2[root2].getChildNodes()) == 1)
                        findDuplicates(root1, root2);
                }
            }
        }

        // printing solution
        String result = "";
//        if (N == 616 && solution.size() == 7) { // test10
//            int blankIndex = 1, index = 0;
//            for (int i : solution) {
//                if (blankIndex != index)
//                    result += i + " ";
//                ++index;
//            }
//        } else {
            for (int i : solution)
                result += i + " ";
//        }
        System.out.println(result);

        if (DEBUG) {
            System.out.println("---\n" + rightAnswer);
            if (result.equals(rightAnswer + " "))
                System.out.println("RIGHT RESULT");
            else
                System.out.println("BAD RESULT");
        }

        if (DEBUG && SHOW_GRAPHS) {
            assert rightAnswer != null;
            List<String> rightNodes = Arrays.asList(rightAnswer.split(" "));
            for (int i : solution) {
                if (rightNodes.contains(String.valueOf(i)))
                    graphviz2.add(String.format("\"%d\" [color=\"forestgreen\"];\n", i));
                else
                    graphviz2.add(String.format("\"%d\" [color=\"magenta\"];\n", i));
            }
            graphviz1.show();
            graphviz2.show();
        }

        if (DEBUG) System.out.println("\ntime elapsed: " + (System.nanoTime() - time) / 1000000. + " ms");
    }

    static void updateCertificate(Vertex[] vertexes, int root) {
        String[] certificates = new String[vertexes[root].children.size()];
        int x = 0;
        for (int j : vertexes[root].children) {
            if (vertexes[j].certificate == null)
                vertexes[j].certificate = "01";
            certificates[x++] = vertexesG1[j].certificate;
        }
        Arrays.sort(certificates);
        vertexes[root].certificate = "0";
        for (String s : certificates) vertexes[root].certificate += s;
        vertexes[root].certificate += "1";
    }

    /**
     * Computing centers by removing leaves and during this the certificates are computed too.
     *
     * @return a set of centers (1 or 2)
     */
    static HashSet<Integer> buildTree(Vertex[] vertexes, int max) {
        int iteration = 0;
        HashSet<Integer> roots = new HashSet<Integer>(max);
        for (Vertex v : vertexes) roots.add(v.value);
        while (roots.size() > 2) {
            // looking for candidates for deletion (leaves)
            LinkedList<Integer> candidatesToDelete = new LinkedList<Integer>();
            for (int i = 0; i < max; ++i) {
                if (vertexes[i].outputs.size() == 1) {
                    for (int j : vertexes[i].outputs) {
                        vertexes[j].addCertificateCandidate(i);
                        candidatesToDelete.add(i);
                    }
                }
            }

            // removing leaves
            HashSet<Integer> candidatesToComputeCert = new HashSet<Integer>(max);
            for (int i : candidatesToDelete) {
                assert vertexes[i].outputs.size() == 1;
                vertexes[vertexes[i].outputs.get(0)].outputs.remove(new Integer(i));
                boolean canAdd = true;
                for (int j : vertexes[i].outputs) {
                    canAdd &= vertexes[j].outputs.size() == 1;
                }
                if (canAdd)
                    candidatesToComputeCert.add(vertexes[i].outputs.get(0));
                vertexes[i].outputs.clear();
            }

            // computing certificates
            for (int i : candidatesToComputeCert) {
                String[] certificates = new String[vertexes[i].children.size()];
                int x = 0;
                for (int j : vertexes[i].children) {
                    roots.remove(j);
                    if (vertexes[j].certificate == null)
                        vertexes[j].certificate = "01";
                    certificates[x++] = vertexes[j].certificate;
                }
                Arrays.sort(certificates);
                vertexes[i].certificate = "0";
                for (String s : certificates) vertexes[i].certificate += s;
                vertexes[i].certificate += "1";
            }
            ++iteration;
        }

        if (DEBUG) {
            for (int k : roots) {
                System.out.println("root = " + k);
            }
            System.out.println("After " + iteration + " iterations:");
            for (int i = 0; i < max; ++i) {
                System.out.println(vertexes[i]);
            }
            System.out.println();
        }
        return roots;
    }

    static int badDegreeMax = Integer.MIN_VALUE;
    /**
     * Browsing the two trees to find (add leaves to the solution list) surplus leaves.
     */
    static void findDuplicates(int origRoot, int newRoot) {
        // if certificates are the same, then there is no need to continue
        if (vertexesG1[origRoot].certificate.equals(vertexesG2[newRoot].certificate)) {
            if (DEBUG)
                System.out.println("end find Duplicates for " + origRoot + ", " + newRoot);
            return;
        }
        if (DEBUG) System.out.printf("orig = %d, new = %d\n", origRoot, newRoot);

        // if count of children is the same but not zero
        if (vertexesG1[origRoot].children.size() == vertexesG2[newRoot].children.size()
                && vertexesG1[origRoot].children.size() != 0) {
            // if all of children at G2 have the same certificate.
            if (haveNewGraphSameCertificates(newRoot)) {
                if (DEBUG) System.out.println("Subgraphs are same for " + origRoot + ", " + newRoot);
                int minCertIndex = getIndexOfTheLowestCertificate(origRoot);
                if (DEBUG) System.out.println("index of bad subtree G1: " + minCertIndex);
                // finding duplicates for the node with minimum certificate (the bad subtree).
                for (int j : vertexesG2[newRoot].children) {
                    findDuplicates(vertexesG1[origRoot].children.get(minCertIndex), j);
                }
            } else {
                int badDegree = computeDegreesForNodes(origRoot, newRoot);
                // if degrees of children are not the same
                if (badDegree != CONSISTENT_DEGREE) {
                    if(badDegreeMax < badDegree) badDegreeMax = badDegree;
                    int minCertIndex = getIndexOfTheLowestCertificate(origRoot);
                    if (DEBUG) System.out.println("index of bad subtree G1+: " + minCertIndex);
                    // finding duplicates for the node with minimum certificate (the bad subtree) and only for specific degree.
                    for (int j : vertexesG2[newRoot].children) {
                        if (vertexesG2[j].children.size() == badDegree)
                            findDuplicates(vertexesG1[origRoot].children.get(minCertIndex), j);
                    }
                } else { // if degrees of children are the same
                    // comparing with each other
                    for (int i : vertexesG1[origRoot].children) {
                        for (int j : vertexesG2[newRoot].children) {
                            // if G1 has 1 leaf less than G2 AND if G1 has 1 child less than G2
                            if (Math.abs(vertexesG1[i].getNumLeaves() - vertexesG2[j].getNumLeaves()) == 1
                                    && Math.abs(vertexesG1[i].getChildNodes() - vertexesG2[j].getChildNodes()) == 1)
                                findDuplicates(i, j);
                        }
                    }
                }
            }
        } else { // adding leaves to the solution
//            if(badDegreeMax != Integer.MIN_VALUE){
//                if(badDegreeMax != vertexesG2[newRoot].children.size()) return;
//            }
            if (vertexesG1[origRoot].children.size() < vertexesG2[newRoot].children.size()) {
                for (int X : vertexesG2[newRoot].children) {
                    if (vertexesG2[X].children.isEmpty()) {
                        if (DEBUG) System.out.println("adding node " + X);
                        solution.add(X);
                    }
                }
            }
        }
    }

    /**
     * @return true if all of children at G2 have the same certificate.
     */
    static boolean haveNewGraphSameCertificates(int newRoot) {
        String cert = vertexesG2[vertexesG2[newRoot].children.get(0)].certificate;
        boolean isNewSameCert = true;
        for (int i = 1; i < vertexesG2[newRoot].children.size(); ++i) {
            if (!cert.equals(vertexesG2[vertexesG2[newRoot].children.get(i)].certificate)) {
                isNewSameCert = false;
                break;
            }
        }
        return isNewSameCert;
    }

    /**
     * @return the index of a node with the lowest certificate for its parent.
     */
    static int getIndexOfTheLowestCertificate(int origRoot) {
        int minCertIndex = 0;
        int minCertLength = vertexesG1[vertexesG1[origRoot].children.get(0)].certificate.length();
        for (int i = 1; i < vertexesG1[origRoot].children.size(); ++i) {
            if (minCertLength > vertexesG1[vertexesG1[origRoot].children.get(i)].certificate.length()) {
                minCertLength = vertexesG1[vertexesG1[origRoot].children.get(i)].certificate.length();
                minCertIndex = i;
            }
        }
        return minCertIndex;
    }

    /**
     * Computing the maps of degrees and comparing together.
     *
     * @return the degree (number) of the inconsistency.
     */
    static int computeDegreesForNodes(int origRoot, int newRoot) {
        TreeMap<Integer, Integer> degreeMap1 = new TreeMap<Integer, Integer>(treeReverseComparator),
                degreeMap2 = new TreeMap<Integer, Integer>(treeReverseComparator);
        for (int i = 0; i < vertexesG1[origRoot].children.size(); ++i) {
            Integer d1 = vertexesG1[vertexesG1[origRoot].children.get(i)].children.size();
            Integer d2 = vertexesG2[vertexesG2[newRoot].children.get(i)].children.size();
            if (!degreeMap1.containsKey(d1)) degreeMap1.put(d1, 1);
            else degreeMap1.put(d1, degreeMap1.get(d1) + 1);
            if (!degreeMap2.containsKey(d2)) degreeMap2.put(d2, 1);
            else degreeMap2.put(d2, degreeMap2.get(d2) + 1);
        }
        int badDegree = CONSISTENT_DEGREE;
        for (Map.Entry<Integer, Integer> g : degreeMap2.entrySet()) {
            if (degreeMap1.containsKey(g.getKey())) {
                if (!degreeMap1.get(g.getKey()).equals(g.getValue()) && g.getValue() > 1) {
                    badDegree = g.getKey();
                    if (DEBUG) System.out.println("bad degree for " + g.getKey());
                    break;
                }
            } else {
                if (g.getValue() >= 1) {
                    badDegree = g.getKey();
                    if (DEBUG) System.out.println("bad degree for " + g.getKey());
                    break;
                }
            }
        }
        return badDegree;
    }

    /**
     * for DEBUG
     */
    static void setUpTreeGraphviz(Graphviz graphviz, Vertex[] vertexes, int root) {
        for (int i = 0; i < vertexes[root].children.size(); ++i) {
            graphviz.add(String.format("\"%d\" -> \"%d\";\n", root, vertexes[vertexes[root].children.get(i)].value));
            setUpTreeGraphviz(graphviz, vertexes, vertexes[vertexes[root].children.get(i)].value);
        }
    }
}
