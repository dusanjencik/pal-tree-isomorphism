package pal;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 * @created 07.11.15
 */
public class Vertex {
    public String certificate;
    public int value;
    public ArrayList<Integer> outputs;
    public ArrayList<Integer> children;
    private int numLeaves = -1, numChildNodes = -1;

    public Vertex(int value) {
        this.value = value;
        outputs = new ArrayList<Integer>();
        children = new ArrayList<Integer>();
    }

    public int getNumLeaves() {
        if (numLeaves == -1) {
            String str = certificate;
            Pattern p = Pattern.compile("01");
            Matcher m = p.matcher(str);
            int count = 0;
            while (m.find()) {
                count += 1;
            }
            numLeaves = count;
        }
        return numLeaves;
    }

    public int getChildNodes() {
        if (numChildNodes == -1) {
            int num = 0;
            for (int i = 0; i < certificate.length(); i++) {
                if (certificate.charAt(i) == '1') {
                    ++num;
                }
            }
            numChildNodes = num;
        }
        return numChildNodes;
    }

    public void addCertificateCandidate(int i) {
        children.add(i);
    }

    public void addOutput(int i) {
        outputs.add(i);
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "certificate='" + certificate + '\'' +
                ", value=" + value +
                ", outputs=" + outputs +
                ", children=" + children +
                '}';
    }
}
