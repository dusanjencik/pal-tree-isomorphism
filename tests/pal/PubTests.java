package pal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Scanner;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 * @created 11.11.15
 */
@RunWith(Parameterized.class)
public class PubTests {

    private static final String PATH_TO_DATASET = "dataset/";

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Parameterized.Parameter
    public String input;
    @Parameterized.Parameter(value = 1)
    public String output;

    @Before
    public void setUpStreams() throws UnsupportedEncodingException {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        System.setIn(new ByteArrayInputStream(input.getBytes("UTF-8")));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(System.out);
        System.setErr(System.err);
        System.setIn(System.in);
    }

    @Parameterized.Parameters
    public static Collection<String[]> data() throws IOException {
        ArrayList<String[]> data = new ArrayList<String[]>(10);
        for (int i = 1; i <= 10; ++i) {
            BufferedReader br = null;
            String in = null;
            try {
                br = new BufferedReader(new FileReader(PATH_TO_DATASET + "pub" + String.format("%02d", i) + ".in"));
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append(System.lineSeparator());
                    line = br.readLine();
                }
                in = sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                assert br != null;
                br.close();
            }

            Scanner in2 = null;
            try {
                in2 = new Scanner(new File(PATH_TO_DATASET + "pub" + String.format("%02d", i) + ".out"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            assert in2 != null;
            String rightAnswer = in2.nextLine();
            in2.close();
            data.add(new String[]{in, rightAnswer});
        }
        return data;
    }

    @Test
    public void testPubs() {
        Main.DEBUG = false;
        Main.SHOW_GRAPHS = false;
        Main.main(new String[]{});
        assertEquals(output.trim(), outContent.toString().trim());
    }
}
